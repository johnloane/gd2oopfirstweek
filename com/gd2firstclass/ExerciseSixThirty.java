
package com.gd2firstclass;

import java.util.ArrayList;
import java.util.Scanner;


public class ExerciseSixThirty {
    public static void main(String[] args)
    {
        ArrayList<Double> sales = new ArrayList<>();
        ArrayList<String> customers = new ArrayList<>();
        populateCustomersAndSales(customers, sales);
        System.out.println(customers);
        System.out.println(sales);
        System.out.println(nameOfBestCustomer(sales, customers));
    }
    
    public static void populateCustomersAndSales(ArrayList<String> customers, ArrayList<Double> sales)
    {
        Scanner sc = new Scanner(System.in);
        String input;
        System.out.println("Please enter the customer names, 0 to mark the end");
        input = sc.next();
        while(!input.equals("0"))
        {
             customers.add(input);  
             input = sc.next();
        }
        
        System.out.println("Please enter the customer sales, 0 to mark the end");
        double spend = sc.nextDouble();
        while(spend != 0.0)
        {
             sales.add(spend);  
             spend = sc.nextDouble();
        }
        sc.close();  
    }
    
    public static String nameOfBestCustomer(ArrayList<Double> sales, ArrayList<String> customers)
    {
        ArrayList<Integer> results = new ArrayList<Integer>();
        results = findIndicesOfHighestSales(sales);
        System.out.println(results);
        String bestCustomers = "";
        if(results.size() > 0)
        {
            for(int i=0; i < results.size(); i++)
            {
                bestCustomers += customers.get(results.get(i)) + " ";
            }
        }
        return bestCustomers;
    }
    
    public static ArrayList<Integer> findIndicesOfHighestSales(ArrayList<Double> sales)
    {
        ArrayList<Integer> results = new ArrayList<Integer>();
        if(results.size() > 0)
        {
            double max = sales.get(0);
            for(int i=1; i < sales.size(); i++)
            {
                if(sales.get(i) > max)
                {
                    max = sales.get(i);
                }
            }
            for(int i = 0; i < sales.size(); i++)
            {
                if(sales.get(i) == max)
                {
                    results.add(i);
                }
            }
        }
        return results;
    }

    
    
}
