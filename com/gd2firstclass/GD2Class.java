//Test what GD2 know about Java
package com.gd2firstclass;

import java.util.Random;


public class GD2Class {

    public static void main(String[] args) {
        gd2TestBasicDataTypes();
        testArray();
        testBitShifting();
        int[] testArray = {1,2,3,4,5,6,7};
        printIntegerArray(testArray);
        swapFirstAndLastIntInArray(testArray);
        printIntegerArray(testArray);
        try
        {
            int largest = findLargestInt(testArray);
            System.out.println("Largest: " + largest);
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            //e.printStackTrace();
            System.out.println("The array is empty. I cannnot find largest in empty array");
        }
       
        int[] emptyArray = {};
        try
        {
            int largest = findLargestInt(emptyArray);
            System.out.println("Largest: " + largest);
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            System.out.println(e.getMessage());
        }
        int sum = findSumIntArray(testArray);
        System.out.println("Sum is: " + sum);
        try
        {
            sum = findSumIntArray(emptyArray);
            System.out.println("Sum empty is: " + sum);
        }
        catch(IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }
        final int ARRAYSIZE = 10;
        int[] randomArray = new int[ARRAYSIZE];
        populateIntArrayWithRandomNumbers(randomArray);
        printIntegerArray(randomArray);
        printEvenIndexIntArray(randomArray);
        printEvenNumbersIntArray(randomArray);
        printIntArrayInReverse(randomArray);
        printFirstAndLastIntArray(randomArray);
        
    }
    
    public static void gd2TestBasicDataTypes()
    {
        //What data types do you know?
        String testString = "Hello";
        boolean trueOrFalse = true;
        System.out.println(Integer.SIZE);
        int biggestInt = Integer.MAX_VALUE;
        int smallestInt = Integer.MIN_VALUE;
        System.out.println("Min is: " + smallestInt 
                + " Max is: " + biggestInt);
        int hugeInt = 2_000_000_000;
        System.out.println(trueOrFalse);
        System.out.println(Long.SIZE);
        System.out.println(Long.MAX_VALUE + " " + Long.MIN_VALUE);
        System.out.println(Character.SIZE);
        //long hugeNumber = 999999999999999999999999999999;
    }
    
    public static void testArray()
    {
        int[] gameScores = new int[100];
        printIntegerArray(gameScores);
    }
    
    public static void printIntegerArray(int[] arrayToPrint)
    {
        for(int i=0; i < arrayToPrint.length; i++)
        {
            if((i+1) % 30 == 0)
            {
                System.out.print(arrayToPrint[i] + "\n");
            }
            else
            {
                System.out.print(arrayToPrint[i] + " ");
            }
        }
        System.out.println("");
    }
    
    public static void testBitShifting()
    {
        int a = 2;
        a = a << 2;
        System.out.println(a);
    }
    //Primitive types are passed by value
    //The function gets a copy of the values, not the values
    //This means that the function can do whatever it wants
    //but once it returns those copies are destroyed 
    //and the original values are as they were
    public static void tryToSwapTwoInts(int a, int b)
    {
        int temp = a;
        a = b;
        b = temp;
    }
    
    public static void swapFirstAndLastIntInArray(int[] 
            testArray)
    {
        int temp = testArray[0];
        testArray[0] = testArray[testArray.length-1];
        testArray[testArray.length-1] = temp;
    }
    
    public static int findLargestInt(int[] intArray)
    {
        if(intArray.length > 0)
        {
            int largest = intArray[0];
            for(int value : intArray)
            {
                if(value > largest)
                {
                    largest = value;
                }
            }
            return largest; 
        }
        else
        {
            throw new ArrayIndexOutOfBoundsException("Array is empty. Can't find largest in empty array");
        }
    }
    
    public static int findSumIntArray(int[] testArray)
    {
        int total = 0;
        if(testArray.length > 0)
        {
            for(int value : testArray)
            {
                total += value;
            }
            return total;
        }
        else
        {
            throw new IllegalArgumentException("The array is empty. Can't sum empty array");
        }
    }
    
    public static void populateIntArrayWithRandomNumbers(int[] arrayToPopulate)
    {
        Random rand = new Random();
        for(int i=0; i < arrayToPopulate.length;i++)
        {
            arrayToPopulate[i] = rand.nextInt(100) + 1;
        }
    }
    
    public static void printEvenIndexIntArray(int[] arrayToPrint)
    {
        for(int i=0; i< arrayToPrint.length;i++)
        {
            if(i % 2 == 0)
            {
                System.out.print(arrayToPrint[i] + " ");
            }
        }
        System.out.println("");
    }
    
    public static void printEvenNumbersIntArray(int[] arrayToPrint)
    {
        for(int value : arrayToPrint)
        {
            if(value % 2 == 0)
            {
                System.out.print(value + " ");
            }
        }
        System.out.println("");
    }
    
    public static void printIntArrayInReverse(int[] arrayToPrint)
    {
        for(int i = arrayToPrint.length-1; i>=0;i--)
        {
             System.out.print(arrayToPrint[i] + " ");
        }
        System.out.println("");
    }
    
    public static void printFirstAndLastIntArray(int[] arrayToPrint)
    {
        System.out.print(arrayToPrint[0] + " " + 
                arrayToPrint[arrayToPrint.length -1] + "\n");
    }
}
