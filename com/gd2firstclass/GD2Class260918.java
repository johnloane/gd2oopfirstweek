
package com.gd2firstclass;

import static com.gd2firstclass.GD2Class.printIntegerArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author John Loane
 */
public class GD2Class260918 
{
    enum Options{CREATE, READ, UPDATE, DELETE;}
    public static void main(String[] args) 
    {
        //testEnums();
//        int testArray[] = {1,2,3,4,5};
//        printIntegerArray(testArray);
//        shallowVDeepCopyArray(testArray);
//        printIntegerArray(testArray);
//        
        ArrayList<String> customers = new ArrayList<>();
        ArrayList<Double> sales = new ArrayList<>();
        getCustomersAndSales(customers, sales);
        System.out.println(customers);
        
    }
    
    public static void testEnums()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter choice: ");
        int input = sc.nextInt();
        Options option = Options.values()[input];
        switch(option)
        {
            case CREATE:
                System.out.println("User wants to create");
                break;
            case READ:
                System.out.println("User wants to read");
                break;
        }                 
    }
    
    public static void shallowVDeepCopyArray(int[] arrayToCopy)
    {
        //This is a shallow copy. Danger here!
//        int[] copyOfArray = arrayToCopy;
//        copyOfArray[0] = 1000000;
        //This is deep copy. It will actually make another copy
        //which we can change without effecting the original
        int[] copyOfArray = Arrays.copyOf(arrayToCopy, arrayToCopy.length);
        copyOfArray[0] = 1000000;
  
    }
    
    public static void printXOs()
    {
        char[][] xoBoard = {{'X', 'O', 'O'}, {'O', 'O', 'X'}, 
            {'X', 'X', 'X'}};
        for(int i=0; i< xoBoard.length; i++)
        {
            for(int j=0; j < xoBoard[0].length; j++)
            {
                System.out.print(xoBoard[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public static void getCustomersAndSales(ArrayList<String>
            customers, ArrayList<Double> sales)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter all customer names, "
                + "enter 0 to indicate you are finished");
        String name = sc.next();
        while(!name.equals("0"))
        {
            customers.add(name);
            name = sc.next();
        }
        //TODO Write and test the code to read in the sales
        //values. HINT: It is pretty much the same
        //as above
    }
    
    
    
    
}
