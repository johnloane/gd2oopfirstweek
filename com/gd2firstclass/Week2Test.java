/*
 * 
 */
package com.gd2firstclass;

import static com.gd2firstclass.GD2Class.printIntegerArray;

import java.util.Arrays;
import java.util.Scanner;



public class Week2Test 
{
    enum Options{CREATE, READ, UPDATE, DELETE;}
    public static void main(String[] args)
    {
        int[] testArray = {1,2,3,4,5,6,7,8,9};
        /*printIntegerArray(testArray);
        copyArray(testArray);
        printIntegerArray(testArray);
        testEnums();*/
        printXsAndOs();
    }
    
    public static void copyArray(int[] arrayToCopy)
    {
        int[] shallowCopyOfArray = arrayToCopy;
        int[] deepCopyOfArray = Arrays.copyOf(arrayToCopy, arrayToCopy.length);
        deepCopyOfArray[1] = 1000000;
        printIntegerArray(deepCopyOfArray);
    }
    
    public static void testEnums()
    {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        Options option = Options.values()[input];
        switch(option)
        {
            case CREATE:
                System.out.println("The user wants to create");
                break;
            case READ:
                System.out.println("The user wants to read");
                break;
        }
    }
    
    public static void printXsAndOs()
    {
        char[][] board = {{'X', 'X', 'O'}, {'O', 'X', 'X'}, {'O', 'O','X'}};
        for(int i=0; i < board[0].length; i++)
        {
            for(int j=0; j < board.length; j++)
            {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
